﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Pet.API.Data;
using Pet.API.Mapper;
using Pet.API.Services;
using System.Collections.Generic;
using System.Globalization;

namespace Pet.API
{
   public class Startup
   {
      public Startup(IConfiguration configuration)
      {
         Configuration = configuration;
      }

      public IConfiguration Configuration { get; }

      public void ConfigureServices(IServiceCollection services)
      {
         services.AddAuthorization(options =>
         {
            options.AddPolicy("NotePolicy", builder => { builder.RequireClaim("scope", "note_api"); });
         });

         services.AddAuthentication("Bearer")
            .AddIdentityServerAuthentication(options =>
            {
               options.Authority = Configuration["Identity:Authority"];
               options.RequireHttpsMetadata = false;
            });

         services.Configure<RequestLocalizationOptions>(options =>
         {
            options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("en-GB");
            options.SupportedCultures = new List<CultureInfo> { new CultureInfo("en-GB"), new CultureInfo("en-GB") };
         });

         services.AddAutoMapper(typeof(AutoMapperConfiguration));

         services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
            .AddJsonOptions(options =>
            {
               options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
               //options.SerializerSettings.DateFormatString = "dd/MM/yyyy";
            });

         services.AddCors(o => o.AddPolicy("AllowAll", builder =>
         {
            builder.AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader();
         }));

         services.AddScoped(provider =>
            new AppDbContext(Configuration["AppSettings:MongoServer"], Configuration["AppSettings:DbNote"]));

         services.AddServicesRegistration();
      }

      public void Configure(IApplicationBuilder app, IHostingEnvironment env)
      {
         if (env.IsDevelopment())
         {
            app.UseDeveloperExceptionPage();
         }

         app.UseCors("AllowAll");

         app.UseAuthentication();

         app.UseMvc();
      }
   }
}