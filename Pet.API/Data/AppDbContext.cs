﻿using MongoDB.Driver;
using Pet.API.Data.Entities;
using Tag = Pet.API.Data.Entities.Tag;

namespace Pet.API.Data
{
   public class AppDbContext
   {
      private readonly IMongoDatabase _mongoDatabase;

      public AppDbContext(string connectionString, string dbName)
      {
         IMongoClient mongoClient = new MongoClient(connectionString);
         _mongoDatabase = mongoClient.GetDatabase(dbName);
      }

      public IMongoCollection<User> Users => _mongoDatabase.GetCollection<User>("users");
      public IMongoCollection<Note> Notes => _mongoDatabase.GetCollection<Note>("notes");
      public IMongoCollection<Tag> NoteTags => _mongoDatabase.GetCollection<Tag>("tags");
   }
}