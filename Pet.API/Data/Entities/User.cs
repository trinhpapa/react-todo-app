﻿using Pet.API.Data.Entities.Extensions;
using Pet.API.Data.Entities.Helper;

namespace Pet.API.Data.Entities
{
   public class User : Extension<string>
   {
      public UserProfile Profile { get; set; } = new UserProfile();
   }
}