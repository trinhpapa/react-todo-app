﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Pet.API.Data.Entities.Extensions;
using Pet.API.Data.Entities.Helper;
using System.Collections.Generic;

namespace Pet.API.Data.Entities
{
   public class Note : Extension<ObjectId>
   {
      [BsonRequired]
      public string UserId { get; set; }

      public NoteDetail Detail { get; set; }

      public List<NoteTask> Tasks { get; set; } = new List<NoteTask>();

      public List<NoteTag> Tags { get; set; } = new List<NoteTag>();
   }
}