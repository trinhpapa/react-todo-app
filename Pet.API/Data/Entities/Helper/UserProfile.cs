﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Pet.API.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Pet.API.Data.Entities.Helper
{
   public class UserProfile
   {
      public UserProfile()
      {
      }

      public UserProfile(string firstName, string lastName)
      {
         FirstName = firstName;
         LastName = lastName;
      }
      public UserProfile(string firstName, string lastName, string email)
      {
         FirstName = firstName;
         LastName = lastName;
         Email = email;
      }

      [StringLength(50)]
      [Required]
      public string FirstName { get; set; }

      [StringLength(50)]
      [Required]
      public string LastName { get; set; }

      [StringLength(1000)]
      public string ImageUrl { get; set; }

      public DateTime? DateOfBirth { get; set; }

      [StringLength(100)]
      public string Email { get; set; }

      [BsonRepresentation(BsonType.String)]
      public UserGender Gender { get; set; }
   }
}