﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Pet.API.Data.Entities.Extensions;
using System.ComponentModel.DataAnnotations;

namespace Pet.API.Data.Entities.Helper
{
   public class NoteTask : Extension<ObjectId>
   {
      public NoteTask()
      {
      }

      public NoteTask(string name)
      {
         Name = name;
      }

      [StringLength(200)]
      public string Name { get; set; }

      [BsonDefaultValue(false)]
      public bool IsChecked { get; set; } = false;
   }
}