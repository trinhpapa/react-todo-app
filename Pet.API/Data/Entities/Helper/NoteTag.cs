﻿namespace Pet.API.Data.Entities.Helper
{
   public class NoteTag
   {
      public NoteTag()
      {
      }

      public NoteTag(string id, string name)
      {
         Id = id;
         Name = name;
      }

      public string Id { get; set; }

      public string Name { get; set; }
   }
}