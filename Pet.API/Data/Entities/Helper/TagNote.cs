﻿using Pet.API.Data.Entities.Extensions;
using System.Collections.Generic;

namespace Pet.API.Data.Entities.Helper
{
   public class TagNote
   {
      public string Id { get; set; }

      public string UserId { get; set; }

      public NoteDetail Detail { get; set; }

      public List<NoteTask> Tasks { get; set; } = new List<NoteTask>();

      public List<NoteTag> Tags { get; set; } = new List<NoteTag>();

      public Meta Meta { get; set; } = new Meta();
   }
}