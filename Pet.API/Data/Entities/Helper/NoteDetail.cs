﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Pet.API.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Pet.API.Data.Entities.Helper
{
   public class NoteDetail
   {
      public NoteDetail()
      {
      }

      public NoteDetail(string imageUrl, string title, string content)
      {
         ImageUrl = imageUrl;
         Title = title;
         Content = content;
      }

      public NoteDetail(string imageUrl, string title, string content, DateTime? eventTime)
      {
         ImageUrl = imageUrl;
         Title = title;
         Content = content;
         EventTime = eventTime;
      }

      public NoteDetail(string imageUrl, string title, string content, DateTime? eventTime, DateTime? reminderTime)
      {
         ImageUrl = imageUrl;
         Title = title;
         Content = content;
         EventTime = eventTime;
         ReminderTime = reminderTime;
      }

      public string ImageUrl { get; set; }

      [MaxLength(500)]
      public string Title { get; set; }

      [MaxLength(1000)]
      public string Content { get; set; }

      public DateTime? EventTime { get; set; }

      public DateTime? ReminderTime { get; set; }

      [BsonRepresentation(BsonType.String)]
      public Color ColorTheme { get; set; }
   }
}