﻿using MongoDB.Bson;
using Pet.API.Data.Entities.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson.Serialization.Attributes;
using Pet.API.Data.Entities.Helper;

namespace Pet.API.Data.Entities
{
   public class Tag : Extension<ObjectId>
   {
      public Tag()
      {
      }

      public Tag(string userId, string name)
      {
         UserId = userId;
         Name = name;
      }

      [BsonRequired]
      public string UserId { get; set; }

      [BsonRequired]
      [StringLength(100)]
      public string Name { get; set; }

      public List<TagNote> Notes { get; set; } = new List<TagNote>();
   }
}