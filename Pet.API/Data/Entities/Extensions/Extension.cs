﻿using MongoDB.Bson.Serialization.Attributes;

namespace Pet.API.Data.Entities.Extensions
{
   public class Extension<T>
   {
      [BsonId]
      public T Id { get; set; }

      public Meta Meta { get; set; } = new Meta();
   }
}