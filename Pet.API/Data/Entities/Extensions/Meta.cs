﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Pet.API.Data.Entities.Extensions
{
   public class Meta
   {
      public DateTime CreatedTime { get; set; }

      public DateTime ModifiedTime { get; set; }

      [BsonDefaultValue(false)]
      public bool IsDeleted { get; set; } = false;

      public DateTime? DeletedTime { get; set; }

      public Meta()
      {
         CreatedTime = ModifiedTime = DateTime.UtcNow;
      }
   }
}