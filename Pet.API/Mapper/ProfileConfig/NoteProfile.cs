﻿using AutoMapper;

namespace Pet.API.Mapper.ProfileConfig
{
   public class NoteProfile : Profile
   {
      public NoteProfile()
      {
         CreateMap<Data.Entities.Note, Models.Note>();
         CreateMap<Data.Entities.Note, Data.Entities.Helper.TagNote>();
         CreateMap<Models.Note, Data.Entities.Note>();
      }
   }
}