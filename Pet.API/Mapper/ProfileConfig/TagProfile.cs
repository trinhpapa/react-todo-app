﻿using AutoMapper;
namespace Pet.API.Mapper.ProfileConfig
{
   public class TagProfile : Profile
   {
      public TagProfile()
      {
         CreateMap<Data.Entities.Tag, Models.Tag>();
         CreateMap<Models.Tag, Data.Entities.Tag>();
         CreateMap<Models.Tag, Data.Entities.Helper.NoteTag>();
      }
   }
}