﻿using AutoMapper;
using Pet.API.Mapper.ProfileConfig;

namespace Pet.API.Mapper
{
   public class AutoMapperConfiguration
   {
      public static MapperConfiguration RegisterMappings()
      {
         return new MapperConfiguration(config =>
         {
            config.AddProfile(new NoteProfile());
            config.AddProfile(new TagProfile());
         });
      }
   }
}