﻿// <License>
//     <Author> Le Hoang Trinh </Author>
//     <Url> http://lehoangtrinh.com </Url>
//     <Project> Pet.API </Project>
//     <File>
//         <Name> NoteService.cs </Name>
//         <Created> 10/8/2019 - 16:52 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using Pet.API.Data;
using Pet.API.Data.Entities;
using Pet.API.Data.Entities.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Internal;
using Tag = Pet.API.Data.Entities.Tag;

namespace Pet.API.Services
{
   public class NoteService
   {
      private readonly IMapper _mapper;
      private readonly AppDbContext _context;
      private readonly TagService _tagService;

      public NoteService(AppDbContext context, IMapper mapper, TagService tagService)
      {
         _context = context;
         _mapper = mapper;
         _tagService = tagService;
      }

      public Task<List<Note>> GetAsync(string userId)
      {
         var filter = Builders<Note>.Filter.Where(w => w.UserId == userId && w.Meta.IsDeleted == false);
         return _context.Notes.Find(filter).ToListAsync();
      }

      public Task<List<Note>> SearchAsync(string userId, string searchString)
      {
         var filter = Builders<Note>.Filter.Where(w => w.UserId == userId && w.Meta.IsDeleted == false && (w.Detail.Title.Contains(searchString) || w.Detail.Content.Contains(searchString)));
         return _context.Notes.Find(filter).ToListAsync();
      }

      public Task<List<Note>> GetRemindersAsync(string userId)
      {
         var filterMeta = Builders<Note>.Filter.Where(w => w.UserId == userId && w.Meta.IsDeleted == false);
         var filterTime = Builders<Note>.Filter.Lte("Detail.ReminderTime", DateTime.UtcNow + TimeSpan.FromHours(24));
         var filterAll = Builders<Note>.Filter.And(filterMeta, filterTime);
         return _context.Notes.Find(filterAll).ToListAsync();
      }

      public Task<Note> GetByIdAsync(string noteId)
      {
         var filter = Builders<Note>.Filter.Eq("_id", ObjectId.Parse(noteId));
         return _context.Notes.Find(filter).FirstOrDefaultAsync();
      }

      public async Task<Models.Note> CreateAsync(Models.Note model)
      {
         var noteEntity = _mapper.Map<Note>(model);
         await _context.Notes.InsertOneAsync(noteEntity);
         var tagNote = _mapper.Map<TagNote>(noteEntity);
         foreach (var tag in model.Tags)
         {
            await _tagService.AppendNoteAsync(tag.Id, tagNote);
         }

         return _mapper.Map<Models.Note>(noteEntity);
      }

      public async Task UpdateAsync(Models.Note model)
      {
         var filterNote = Builders<Note>.Filter.Eq(w => w.Id, ObjectId.Parse(model.Id));
         var updateNote = Builders<Note>.Update.Set(w => w.Detail.Title, model.Detail.Title)
            .Set(w => w.Detail.Content, model.Detail.Content)
            .Set(w => w.Detail.ReminderTime, model.Detail.ReminderTime);


         var filterNoteTag = Builders<Tag>.Filter.Where(w => w.Notes.Any(i => i.Id == model.Id));
         var updateNoteTag = Builders<Tag>.Update.Set(w => w.Notes[-1].Detail.Title, model.Detail.Title)
            .Set(w => w.Notes[-1].Detail.Content, model.Detail.Content)
            .Set(w => w.Notes[-1].Detail.ReminderTime, model.Detail.ReminderTime);

         if (!string.IsNullOrEmpty(model.Detail.ImageUrl))
         {
            updateNote.Set(w => w.Detail.ImageUrl, model.Detail.ImageUrl);
            updateNoteTag.Set(w => w.Notes[-1].Detail.ImageUrl, model.Detail.ImageUrl);
         }

         await Task.WhenAll(_context.Notes.FindOneAndUpdateAsync(filterNote, updateNote),
            _context.NoteTags.UpdateManyAsync(filterNoteTag, updateNoteTag));
      }

      public async Task ToTrashAsync(string noteId)
      {
         var filterNote = Builders<Note>.Filter.Eq(w => w.Id, ObjectId.Parse(noteId));
         var updateNote = Builders<Note>.Update.Set(w => w.Meta.IsDeleted, true).Set(w => w.Meta.DeletedTime, DateTime.UtcNow);

         var filterNoteTag = Builders<Tag>.Filter.Where(w => w.Notes.Any(i => i.Id == noteId));
         var updateNoteTag = Builders<Tag>.Update.Set(w => w.Notes[-1].Meta.IsDeleted, true).Set(w => w.Notes[-1].Meta.DeletedTime, DateTime.UtcNow);

         await Task.WhenAll(_context.Notes.FindOneAndUpdateAsync(filterNote, updateNote),
            _context.NoteTags.UpdateManyAsync(filterNoteTag, updateNoteTag));
      }

      public Task<List<Note>> GetTrashAsync(string userId)
      {
         var filter = Builders<Note>.Filter.Where(w => w.UserId == userId && w.Meta.IsDeleted);
         return _context.Notes.Find(filter).ToListAsync();
      }

      public async Task RestoreAsync(string noteId)
      {
         var filterNote = Builders<Note>.Filter.Eq(w => w.Id, ObjectId.Parse(noteId));
         var updateNote = Builders<Note>.Update.Set(w => w.Meta.IsDeleted, false);

         var filterNoteTag = Builders<Tag>.Filter.Where(w => w.Notes.Any(i => i.Id == noteId));
         var updateNoteTag = Builders<Tag>.Update.Set(w => w.Notes[-1].Meta.IsDeleted, false);

         await Task.WhenAll(_context.Notes.FindOneAndUpdateAsync(filterNote, updateNote),
            _context.NoteTags.UpdateManyAsync(filterNoteTag, updateNoteTag));

      }

      public async Task DeleteAsync(string noteId)
      {
         var filterNote = Builders<Note>.Filter.Eq("_id", ObjectId.Parse(noteId));
         var filterNoteTag = Builders<Tag>.Filter.Where(w => w.Notes.Any(i => i.Id == noteId));
         var pullNoteTag = Builders<Tag>.Update.PullFilter(w => w.Notes, x => x.Id == noteId);
         await Task.WhenAll(_context.Notes.FindOneAndDeleteAsync(filterNote),
            _context.NoteTags.UpdateManyAsync(filterNoteTag, pullNoteTag));
      }
   }
}