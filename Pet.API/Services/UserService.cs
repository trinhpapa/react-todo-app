﻿// <License>
//     <Author> Le Hoang Trinh </Author>
//     <Url> http://lehoangtrinh.com </Url>
//     <Project> Pet.API </Project>
//     <File>
//         <Name> UserService.cs </Name>
//         <Created> 10/8/2019 - 16:52 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Pet.API.Data;
using Pet.API.Data.Entities;
using System.Threading.Tasks;
using Pet.API.Data.Entities.Helper;

namespace Pet.API.Services
{
   public class UserService
   {
      private readonly AppDbContext _context;

      public UserService(AppDbContext context)
      {
         _context = context;
      }

      public Task CreateAsync(string id, string firstname, string lastname)
      {
         var userEntity = new User()
         {
            Id = id,
            Profile = new UserProfile(firstname, lastname)
         };

         return _context.Users.InsertOneAsync(userEntity);
      }

      public Task CreateAsync(string id, string firstname, string lastname, string email)
      {
         var userEntity = new User()
         {
            Id = id,
            Profile = new UserProfile(firstname, lastname, email)
         };

         return _context.Users.InsertOneAsync(userEntity);
      }

      public Task CreateAsync(Models.User model)
      {
         var userEntity = new User()
         {
            Id = model.Id,
            Profile = new UserProfile(model.FirstName, model.LastName, model.Email)
         };

         return _context.Users.InsertOneAsync(userEntity);
      }
   }
}