﻿// <License>
//     <Author> Le Hoang Trinh </Author>
//     <Url> http://lehoangtrinh.com </Url>
//     <Project> Pet.API </Project>
//     <File>
//         <Name> ServicesRegistration.cs </Name>
//         <Created> 10/8/2019 - 16:52 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Microsoft.Extensions.DependencyInjection;

namespace Pet.API.Services
{
   public static class ServicesRegistration
   {
      public static IServiceCollection AddServicesRegistration(this IServiceCollection services)
      {
         services.AddTransient<NoteService>();
         services.AddTransient<UserService>();
         services.AddTransient<TagService>();

         return services;
      }
   }
}