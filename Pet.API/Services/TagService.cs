﻿// <License>
//     <Author> Le Hoang Trinh </Author>
//     <Url> http://lehoangtrinh.com </Url>
//     <Project> Pet.API </Project>
//     <File>
//         <Name> TagService.cs </Name>
//         <Created> 10/8/2019 - 16:52 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using Pet.API.Data;
using Pet.API.Data.Entities.Helper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pet.API.Data.Entities;
using Tag = Pet.API.Data.Entities.Tag;

namespace Pet.API.Services
{
   public class TagService
   {
      private readonly IMapper _mapper;
      private readonly AppDbContext _context;

      public TagService(AppDbContext context, IMapper mapper)
      {
         _context = context;
         _mapper = mapper;
      }

      public Task<List<Tag>> GetAsync(string userId)
      {
         var filter = Builders<Tag>.Filter.Where(w => w.UserId == userId && w.Meta.IsDeleted == false);
         return _context.NoteTags.Find(filter).ToListAsync();
      }

      public Task<Tag> GetByIdAsync(string tagId)
      {
         var filter = Builders<Tag>.Filter.Eq(w => w.Id, ObjectId.Parse(tagId));
         return _context.NoteTags.Find(filter).FirstOrDefaultAsync();
      }

      public async Task<Models.Tag> CreateAsync(Models.Tag model)
      {
         var tagEntity = _mapper.Map<Tag>(model);
         await _context.NoteTags.InsertOneAsync(tagEntity);
         return _mapper.Map<Models.Tag>(tagEntity);
      }

      public async Task UpdateAsync(Models.Tag model)
      {
         var filterTag = Builders<Tag>.Filter.Eq(w => w.Id, ObjectId.Parse(model.Id));
         var updateTag = Builders<Tag>.Update.Set(w => w.Name, model.Name);

         var filterNote = Builders<Note>.Filter.Where(w => w.Tags.Any(i => i.Id == model.Id));
         var updateNote = Builders<Note>.Update.Set(w => w.Tags[-1].Name, model.Name);

         await Task.WhenAll(_context.NoteTags.FindOneAndUpdateAsync(filterTag, updateTag),
            _context.Notes.UpdateManyAsync(filterNote, updateNote));
      }

      public async Task DeleteAsync(string tagId)
      {
         var filterTag = Builders<Tag>.Filter.Eq(w => w.Id, ObjectId.Parse(tagId));
         var filterNote = Builders<Note>.Filter.Where(w => w.Tags.Any(i => i.Id == tagId));
         var pullNote = Builders<Note>.Update.PullFilter(w => w.Tags, x => x.Id == tagId);
         var filterNoteTag = Builders<Tag>.Filter.Where(w => w.Notes.Any(i => i.Tags.Any(e => e.Id == tagId)));
         var pullNoteTag = Builders<Tag>.Update.PullFilter(w => w.Notes[-1].Tags, e => e.Id == tagId);
         await Task.WhenAll(_context.NoteTags.FindOneAndDeleteAsync(filterTag),
            _context.NoteTags.UpdateManyAsync(filterNoteTag, pullNoteTag),
            _context.Notes.UpdateManyAsync(filterNote, pullNote));
      }

      public async Task DeleteMultipleAsync(List<string> tagIds)
      {
         foreach (var tagId in tagIds)
         {
            await DeleteAsync(tagId);
         }
      }

      public Task AppendNoteAsync(string tagId, TagNote note)
      {
         var filter = Builders<Tag>.Filter.Eq(w => w.Id, ObjectId.Parse(tagId));
         var update = Builders<Tag>.Update.Push($"{nameof(Tag.Notes)}", note);
         return _context.NoteTags.UpdateOneAsync(filter, update);
      }
   }
}