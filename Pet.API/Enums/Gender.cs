﻿// <License>
//     <Author> Le Hoang Trinh </Author>
//     <Url> http://lehoangtrinh.com </Url>
//     <Project> Pet.API </Project>
//     <File>
//         <Name> Gender.cs </Name>
//         <Created> 9/8/2019 - 17:04 </Created>
//     </File>
//     <Summary></Summary>
// <License>

namespace Pet.API.Enums
{
   public enum UserGender
   {
      Male,
      Female,
      Other
   }
}