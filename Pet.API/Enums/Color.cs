﻿// <License>
//     <Author> Le Hoang Trinh </Author>
//     <Url> http://lehoangtrinh.com </Url>
//     <Project> Pet.API </Project>
//     <File>
//         <Name> Color.cs </Name>
//         <Created> 9/8/2019 - 17:24 </Created>
//     </File>
//     <Summary></Summary>
// <License>

namespace Pet.API.Enums
{
   public enum Color
   {
      Red,
      Blue,
      Green,
      Gray
   }
}