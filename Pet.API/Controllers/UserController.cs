﻿// <License>
//     <Author> Le Hoang Trinh </Author>
//     <Url> http://lehoangtrinh.com </Url>
//     <Project> Pet.API </Project>
//     <File>
//         <Name> UserController.cs </Name>
//         <Created> 10/8/2019 - 16:52 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Microsoft.AspNetCore.Mvc;
using Pet.API.Models;
using Pet.API.Services;
using System.Threading.Tasks;

namespace Pet.API.Controllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class UserController : ControllerBase
   {
      private readonly UserService _userService;

      public UserController(UserService userService)
      {
         _userService = userService;
      }

      [HttpPost]
      public async Task<IActionResult> CreateAsync([FromBody]User model)
      {
         if (!ModelState.IsValid) return BadRequest();
         await _userService.CreateAsync(model);
         return Ok();
      }
   }
}