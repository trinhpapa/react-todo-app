﻿// <License>
//     <Author> Le Hoang Trinh </Author>
//     <Url> http://lehoangtrinh.com </Url>
//     <Project> Pet.API </Project>
//     <File>
//         <Name> TagController.cs </Name>
//         <Created> 10/8/2019 - 16:52 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Pet.API.Services;
using System.Threading.Tasks;

namespace Pet.API.Controllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class TagController : ControllerBase
   {
      private readonly TagService _tagService;

      public TagController(TagService tagService)
      {
         _tagService = tagService;
      }

      public async Task<IActionResult> GetAsync()
      {
         var userId = User.FindFirst("sub").Value;
         var data = await _tagService.GetAsync(userId);
         return new JsonResult(data);
      }

      [HttpGet("{id}")]
      public async Task<IActionResult> GetAsync(string id)
      {
         var data = await _tagService.GetByIdAsync(id);
         return new JsonResult(data);
      }

      [HttpPost]
      public async Task<IActionResult> CreateAsync([FromBody]Models.Tag model)
      {
         if (!ModelState.IsValid) return BadRequest();
         model.UserId = User.FindFirst("sub").Value;
         var data = await _tagService.CreateAsync(model);
         return new JsonResult(data);
      }

      [HttpDelete("{id}")]
      public async Task<IActionResult> DeleteAsync(string id)
      {
         await _tagService.DeleteAsync(id);
         return Ok();
      }

      [HttpDelete("list")]
      public async Task<IActionResult> DeleteMultipleAsync([FromBody]List<string> ids)
      {
         await _tagService.DeleteMultipleAsync(ids);
         return Ok();
      }
   }
}