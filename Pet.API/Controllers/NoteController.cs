﻿// <License>
//     <Author> Le Hoang Trinh </Author>
//     <Url> http://lehoangtrinh.com </Url>
//     <Project> Pet.API </Project>
//     <File>
//         <Name> NoteController.cs </Name>
//         <Created> 10/8/2019 - 16:52 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Pet.API.Services;
using System;
using System.Threading.Tasks;

namespace Pet.API.Controllers
{
   [Route("api/[controller]")]
   [ApiController]
   [Authorize(Policy = "NotePolicy")]
   public class NoteController : ControllerBase
   {
      private readonly NoteService _noteService;

      public NoteController(NoteService noteService)
      {
         _noteService = noteService;
      }

      public async Task<IActionResult> GetAsync()
      {
         var userId = User.FindFirst("sub").Value;
         var data = await _noteService.GetAsync(userId);
         return new JsonResult(data);
      }

      [HttpGet("reminder")]
      public async Task<IActionResult> ReminderAsync()
      {
         var userId = User.FindFirst("sub").Value;
         var data = await _noteService.GetRemindersAsync(userId);
         return new JsonResult(data);
      }

      [HttpGet("{id}")]
      public async Task<IActionResult> GetByIdAsync(string id)
      {
         var data = await _noteService.GetByIdAsync(id);
         return new JsonResult(data);
      }

      [HttpGet("search")]
      public async Task<IActionResult> SearchAsync([FromQuery]string s)
      {
         s = Uri.UnescapeDataString(s);
         var userId = User.FindFirst("sub").Value;
         var data = await _noteService.SearchAsync(userId, s);
         return new JsonResult(data);
      }

      [HttpPost]
      public async Task<IActionResult> CreateAsync([FromBody]Models.Note model)
      {
         if (!ModelState.IsValid) return BadRequest();
         model.UserId = User.FindFirst("sub").Value;
         var data = await _noteService.CreateAsync(model);
         return new JsonResult(data);
      }

      [HttpPut]
      public async Task<IActionResult> UpdateAsync([FromBody]Models.Note model)
      {
         if (!ModelState.IsValid) return BadRequest();
         await _noteService.UpdateAsync(model);
         return Ok();
      }

      [HttpDelete("{id}")]
      public async Task<IActionResult> ToTrashAsync(string id)
      {
         await _noteService.ToTrashAsync(id);
         return Ok();
      }
   }
}