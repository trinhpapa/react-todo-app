﻿// <License>
//     <Author> Le Hoang Trinh </Author>
//     <Url> http://lehoangtrinh.com </Url>
//     <Project> Pet.API </Project>
//     <File>
//         <Name> TrashController.cs </Name>
//         <Created> 14/8/2019 - 09:22 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Microsoft.AspNetCore.Mvc;
using Pet.API.Services;
using System.Threading.Tasks;

namespace Pet.API.Controllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class TrashController : ControllerBase
   {
      private readonly NoteService _noteService;

      public TrashController(NoteService noteService)
      {
         _noteService = noteService;
      }

      public async Task<IActionResult> GetAsync()
      {
         var userId = User.FindFirst("sub").Value;
         var data = await _noteService.GetTrashAsync(userId);
         return new JsonResult(data);
      }

      [HttpPut("{id}")]
      public async Task<IActionResult> RestoreAsync(string id)
      {
         await _noteService.RestoreAsync(id);
         return Ok();
      }

      [HttpDelete("{id}")]
      public async Task<IActionResult> DeleteAsync(string id)
      {
         await _noteService.DeleteAsync(id);
         return Ok();
      }
   }
}