﻿using Pet.API.Enums;

namespace Pet.API.Models
{
   public class User
   {
      public string Id { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public UserGender Gender { get; set; }
      public string Email { get; set; }
   }
}