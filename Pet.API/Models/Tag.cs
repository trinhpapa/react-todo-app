﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pet.API.Models
{
   public class Tag
   {
      public string Id { get; set; }

      public string UserId { get; set; }

      [StringLength(100)]
      public string Name { get; set; }

      public List<Note> Notes { get; set; } = new List<Note>();
   }
}