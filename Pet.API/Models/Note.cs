﻿using Pet.API.Data.Entities;
using Pet.API.Data.Entities.Extensions;
using System.Collections.Generic;
using Pet.API.Data.Entities.Helper;

namespace Pet.API.Models
{
   public class Note
   {
      public string Id { get; set; }

      public string UserId { get; set; }

      public NoteDetail Detail { get; set; }

      public List<NoteTask> Tasks { get; set; }

      public List<NoteTag> Tags { get; set; }

      public Meta Meta { get; set; } = new Meta();
   }
}