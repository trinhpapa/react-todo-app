import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "./core/extensionHeplper";
import App from "./containers/App";
import "font-awesome/css/font-awesome.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import store from "./store";
import { Provider } from "react-redux";

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
