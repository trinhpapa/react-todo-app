import { FETCH_TRASHS, RESTORE_TRASH } from "../actions";

const initialState = {
  data: [],
  item: {},
  page: 0
};

export function trashsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_TRASHS:
      return { ...state, data: action.trashs };

    case RESTORE_TRASH:
      state.data.splice(state.data.findIndex(e => e.id === action.id), 1);
      return { ...state };

    default:
      return { ...state };
  }
}
