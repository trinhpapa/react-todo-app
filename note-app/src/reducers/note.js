import {
  FETCH_NOTES,
  REMOVE_NOTES,
  ADD_NOTE,
  UPDATE_NOTE,
  FETCH_REMINDER,
  SEARCH_NOTE
} from "../actions";

const initialState = {
  data: [],
  item: {},
  page: 0
};

export function notesReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_NOTES:
      return { ...state, data: action.notes };

    case FETCH_REMINDER:
      return { ...state, data: action.notes };

    case SEARCH_NOTE:
      return { ...state, data: action.notes };

    case REMOVE_NOTES:
      state.data.splice(state.data.findIndex(e => e.id === action.id), 1);
      return { ...state };

    case ADD_NOTE:
      state.data.push(action.note);
      return { ...state };

    case UPDATE_NOTE:
      const index = state.data.findIndex(e => e.id === action.note.id);
      state.data[index].title = action.note.title;
      state.data[index].content = action.note.content;
      return { ...state };

    default:
      return { ...state };
  }
}
