import {
  CREATE_TAG,
  DELETE_TAG,
  FETCH_TAG,
  UPDATE_TAG,
  GET_BYID
} from "../actions";

const initialState = {
  data: [],
  item: {},
  page: 0
};

export function tagsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_TAG:
      return { ...state, data: action.tags };

    case GET_BYID:
      return { ...state, item: action.tag };

    case DELETE_TAG:
      action.ids.forEach(id => {
        state.data.splice(state.data.findIndex(e => e.id === id), 1);
      });
      return { ...state };

    case CREATE_TAG:
      state.data.push(action.tag);
      return { ...state };

    case UPDATE_TAG:
      const index = state.data.findIndex(e => e.id === action.note.id);
      state.data[index].title = action.note.title;
      state.data[index].content = action.note.content;
      return { ...state };

    default:
      return { ...state };
  }
}
