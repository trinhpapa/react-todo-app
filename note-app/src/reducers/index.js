import { combineReducers } from "redux";
import { notesReducer } from "./note";
import { trashsReducer } from "./trash";
import { tagsReducer } from "./tag";

export default combineReducers({
  notes: notesReducer,
  trashs: trashsReducer,
  tags: tagsReducer
});
