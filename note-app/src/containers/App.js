import React from "react";
import logo from "../bot.png";
import "./App.css";
import "../core/extensionHeplper";
import * as Constant from "../core/Constants";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import Note from "../components/note/Note";
import Trash from "../components/trash/Trash";
import NavMenu from "../components/nav-menu/NavMenu";
import Login from "../components/login/Login";
import Tag from "../components/tag/Tag";
import Reminder from "../components/reminder/Reminder";
import { connect } from "react-redux";
import { fetchTagRequest } from "../actions";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fullname: "",
      modalEditTag: false
    };
  }

  componentDidMount() {
    if (window.localStorage.getItem(Constant.USER_STORAGE) !== null) {
      const userData = window.localStorage
        .getItem(Constant.USER_STORAGE)
        .decodeUserInfo();
      this.setState({
        fullname: `${userData.family_name} ${userData.given_name}`
      });
      this.props.fetchTag();
    }
  }

  onLogoutClick = () => {
    window.localStorage.removeItem(Constant.USER_STORAGE);
    window.location.reload();
  };

  render() {
    return (
      <BrowserRouter>
        {(() => {
          switch (window.localStorage.getItem(Constant.USER_STORAGE)) {
            case null:
              return (
                <div className="App">
                  <Redirect to="/login" />
                  <Route path="/login" component={Login} />
                </div>
              );

            default:
              return (
                <div className="App">
                  <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <div className="userControl">
                      <button
                        className="logoutButton"
                        onClick={this.onLogoutClick}
                      >
                        <i className="fa fa-power-off" />
                      </button>
                      <span className="userFullName">
                        {this.state.fullname}
                      </span>
                    </div>
                    <NavMenu data={this.props.tags.data} />
                    <button className="toggle-header">
                      <i className="fa fa-bars" />
                    </button>
                  </header>
                  <div className="main-route-place">
                    <Redirect to="/" />
                    <Route
                      exact
                      path="/"
                      render={() => <Redirect to="/note" />}
                    />
                    <Route path="/note" component={Note} />
                    <Route path="/reminders" component={Reminder} />
                    <Route exact path="/tag/:tagId" component={Tag} />
                    <Route path="/trash" component={Trash} />
                  </div>
                </div>
              );
          }
        })()}
      </BrowserRouter>
    );
  }
}

const mapTagStateToProps = state => {
  return {
    tags: state.tags
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchTag: () => {
      dispatch(fetchTagRequest());
    }
  };
};

export default connect(
  mapTagStateToProps,
  mapDispatchToProps
)(App);
