import { ApiSettings, ImgurApi } from "./Constants";
import * as Constant from "./Constants";

export function resourceRequest(controller = "", type = "GET", data = {}) {
  const access_token = window.localStorage
    .getItem(Constant.USER_STORAGE)
    .decodeUserInfo().auth.access_token;
  data = data === null ? undefined : JSON.stringify(data);
  const url = `${ApiSettings.API_RESOURCE}${controller}`;
  return fetch(url, {
    method: type,
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${access_token}`
    },
    redirect: "follow",
    referrer: "no-referrer",
    body: data
  });
}

export function authRequest(action = "", type = "GET", data, access_token) {
  data = data === null ? undefined : new URLSearchParams(data);
  const url = `${ApiSettings.API_AUTH}${action}`;
  return fetch(url, {
    method: type,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: `Bearer ${access_token}`
    },
    body: data
  });
}

export function baseRequest(action = "", type = "GET", data = {}) {
  data = data === null ? undefined : JSON.stringify(data);
  const url = `${ApiSettings.API_AUTH}${action}`;
  return fetch(url, {
    method: type,
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json"
    },
    redirect: "follow",
    referrer: "no-referrer",
    body: data
  });
}

export function imgruUploadRequest(action = "3/image", imageBase64) {
  const data = {
    image: imageBase64.replace(/.*,/, ""),
    type: "base64"
  };
  const body_data = new URLSearchParams(data);
  const url = `${ImgurApi.IMGUR_HOST}${action}`;
  const auth = `Client-ID ${ImgurApi.IMGUR_CLIENT_ID}`;
  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: auth
    },
    body: body_data
  });
}
