Object.assign(String.prototype, {
  toDateTime() {
    if (this === null || this === undefined) return "";
    let dateTime = new Date(this);
    let day = dateTime.getDate();
    let month = dateTime.getMonth() + 1;
    let year = dateTime.getFullYear();
    let hours = dateTime.getHours();
    let minute = dateTime.getMinutes();
    return `${day < 10 ? "0" + day : day}/${
      month < 10 ? "0" + month : month
    }/${year} - ${hours < 10 ? "0" + hours : hours}:${
      minute < 10 ? "0" + minute : minute
    }`;
  },

  toTime() {
    if (this === null || this === undefined) return "";
    let dateTime = new Date(this);
    let hours = dateTime.getHours();
    let minute = dateTime.getMinutes();
    return `${hours < 10 ? "0" + hours : hours}:${
      minute < 10 ? "0" + minute : minute
    }`;
  },

  toDate() {
    if (this === null || this === undefined) return "";
    let dateTime = new Date(this);
    let day = dateTime.getDate();
    let month = dateTime.getMonth() + 1;
    let year = dateTime.getFullYear();
    return `${day < 10 ? "0" + day : day}/${
      month < 10 ? "0" + month : month
    }/${year}`;
  },

  encodeUserInfo() {
    const buf_1 = new Buffer(this.toString());
    var string_1 = buf_1.toString("base64") + "AabhjqAnhTrinhDaiCaANCJHsbcb";
    const buf_2 = new Buffer(string_1.toString());
    return buf_2.toString("base64");
  },

  decodeUserInfo() {
    const buf_1 = new Buffer(this.toString(), "base64");
    const string_1 = buf_1
      .toString()
      .replace("AabhjqAnhTrinhDaiCaANCJHsbcb", "");
    const buf_2 = new Buffer(string_1.toString(), "base64");
    return JSON.parse(buf_2.toString());
  }
});
