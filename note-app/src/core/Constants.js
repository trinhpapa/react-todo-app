const API_RESOURCE = "http://react.lehoangtrinh.com/api/";
// const API_RESOURCE = "http://localhost:51269/api/";
const API_AUTH = "http://auth.lehoangtrinh.com/";
// const API_AUTH = "http://localhost:55555/";
const SECRET_KEY = "91d0dbc7898ec8f66fec0051d87a1d0a";
export const ApiSettings = {
  API_RESOURCE,
  API_AUTH,
  SECRET_KEY
};

const GET_NOTES = "GET_NOTES";
const GET_NOTE = "GET_NOTE";
const ADD_NOTE = "ADD_NOTE";
const UPDATE_NOTE = "UPDATE_NOTE";
const REMOVE_NOTE = "REMOVE_NOTE";
const RESTORE_NOTE = "RESTORE_NOTE";
export const ActionTypes = {
  GET_NOTE,
  GET_NOTES,
  ADD_NOTE,
  UPDATE_NOTE,
  REMOVE_NOTE,
  RESTORE_NOTE
};

const IMGUR_HOST = "https://api.imgur.com/";
const IMGUR_CLIENT_ID = "426f7ffdba30bd3";
const IMGUR_CLIENT_SECRET = "3d4eb98a2704b2a25185410d94b57477a7c11fda";
export const ImgurApi = {
  IMGUR_HOST,
  IMGUR_CLIENT_ID,
  IMGUR_CLIENT_SECRET
};

export const USER_STORAGE = "_I'mSuperHeroHoho";
export const INVALID_USERNAME_PASSWORD =
  "Tên đăng nhập hoặc mật khẩu không đúng";
export const INVALID_INPUT_ERROR = "Các thông tin không được để trống";
export const LOGIN_ERROR = "Đăng nhập không thành công";
export const REPASSWORD_DONTMATCH = "Mật khẩu không trùng khớp";
