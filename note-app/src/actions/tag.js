import { resourceRequest } from "../core/apiHelper.js";

export const FETCH_TAG = "FETCH_TAG";
export const GET_BYID = "GET_BYID";
export const CREATE_TAG = "CREATE_TAG";
export const UPDATE_TAG = "UPDATE_TAG";
export const DELETE_TAG = "DELETE_TAG";

export function fetchTagRequest() {
  return dispatch => {
    return resourceRequest("tag", "GET", null)
      .then(res => res.json())
      .then(res => {
        dispatch(fetchTag(res));
      })
      .catch(err => console.log(err));
  };
}

export function fetchTag(tags) {
  return {
    type: FETCH_TAG,
    tags
  };
}

export function getTagByIdRequest(id) {
  return dispatch => {
    return resourceRequest(`tag/${id}`, "GET", null)
      .then(res => res.json())
      .then(res => {
        dispatch(getById(res));
      })
      .catch(err => console.log(err));
  };
}

export function getById(tag) {
  return {
    type: GET_BYID,
    tag
  };
}

export function createTagRequest(tag) {
  return dispatch => {
    return resourceRequest("tag", "POST", tag)
      .then(res => res.json())
      .then(res => {
        dispatch(createTag(res));
      })
      .catch(err => console.log(err));
  };
}

export function createTag(tag) {
  return {
    type: CREATE_TAG,
    tag
  };
}

export function updateTagRequest(tag) {
  return dispatch => {
    return resourceRequest("tag", "GET", null)
      .then(res => {
        dispatch(updateTag(tag));
      })
      .catch(err => console.log(err));
  };
}

export function updateTag(tag) {
  return {
    type: UPDATE_TAG,
    tag
  };
}

export function deleteTagRequest(ids) {
  return dispatch => {
    return resourceRequest(`tag/list`, "DELETE", ids)
      .then(res => {
        dispatch(deleteTag(ids));
      })
      .catch(err => console.log(err));
  };
}

export function deleteTag(ids) {
  return {
    type: DELETE_TAG,
    ids
  };
}
