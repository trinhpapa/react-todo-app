import { authRequest, baseRequest } from "../core/apiHelper";

export const userLoginRequest = user => {
  var loginData = new FormData();
  loginData.append("client_id", "note_client");
  loginData.append("client_secret", "trinh_dai_ca");
  loginData.append("grant_type", "password");
  loginData.append("scope", "note_api openid profile");
  loginData.append("response_type", "token id_token");
  loginData.append("username", user.username);
  loginData.append("password", user.password);

  return authRequest("connect/token", "POST", loginData, null).then(authRes =>
    authRes.json()
  );
};

export const userInfoRequest = access_token => {
  return authRequest("connect/userinfo", "GET", null, access_token).then(
    infoRes => infoRes.json()
  );
};

export const userRegisterRequest = user => {

  return baseRequest("api/account", "POST", user, null).then(regisRes =>
    regisRes.json()
  );
};
