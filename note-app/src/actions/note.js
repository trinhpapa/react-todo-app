import { resourceRequest, imgruUploadRequest } from "../core/apiHelper";

export const FETCH_NOTES = "FETCH_NOTES";
export const REMOVE_NOTES = "REMOVE_NOTES";
export const ADD_NOTE = "ADD_NOTE";
export const UPDATE_NOTE = "UPDATE_NOTE";
export const SEARCH_NOTE = "SEARCH_NOTE";
export const FETCH_REMINDER = "FETCH_REMINDER";

export const fetchNotesRequest = () => {
  return dispatch => {
    return resourceRequest("note", "GET", null)
      .then(res => res.json())
      .then(res => {
        dispatch(fetchNotes(res));
      })
      .catch(res => console.log(res));
  };
};

export const fetchNotes = notes => {
  return {
    type: FETCH_NOTES,
    notes
  };
};

export const searchNotesRequest = searchString => {
  searchString = encodeURI(searchString);
  return dispatch => {
    return resourceRequest(`note/search?s=${searchString}`, "GET", null)
      .then(res => res.json())
      .then(res => {
        dispatch(fetchNotes(res));
      })
      .catch(res => console.log(res));
  };
};

export const searchNotes = notes => {
  return {
    type: SEARCH_NOTE,
    notes
  };
};

export const fetchRemindersRequest = () => {
  return dispatch => {
    return resourceRequest("note/reminder", "GET", null)
      .then(res => res.json())
      .then(res => {
        dispatch(fetchNotes(res));
      })
      .catch(res => console.log(res));
  };
};

export const fetchReminders = notes => {
  return {
    type: FETCH_REMINDER,
    notes
  };
};

export const removeNoteRequest = id => {
  return dispatch => {
    return resourceRequest(`note/${id}`, "DELETE", null)
      .then(res => {
        dispatch(removeNotes(id));
      })
      .catch(res => console.log(res));
  };
};

export const removeNotes = id => {
  return {
    type: REMOVE_NOTES,
    id
  };
};

export const addNoteRequest = note => {
  return dispatch => {
    return resourceRequest(`note`, "POST", note)
      .then(res => res.json())
      .then(res => {
        dispatch(addNote(res));
      })
      .catch(res => console.log(res));
  };
};

export const addNote = note => {
  return {
    type: ADD_NOTE,
    note
  };
};

export const updateNoteRequest = note => {
  return dispatch => {
    return resourceRequest(`note`, "PUT", note)
      .then(res => {
        dispatch(updateNote(note));
      })
      .catch(res => console.log(res));
  };
};

export const updateNote = note => {
  return {
    type: UPDATE_NOTE,
    note
  };
};

export const uploadImgRuImage = imageData => {
  return imgruUploadRequest("3/image", imageData).then(res => res.json());
};
