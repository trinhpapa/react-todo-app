import {
  fetchNotesRequest,
  fetchRemindersRequest,
  removeNoteRequest,
  addNoteRequest,
  updateNoteRequest,
  uploadImgRuImage,
  searchNotesRequest,
  FETCH_NOTES,
  SEARCH_NOTE,
  FETCH_REMINDER,
  REMOVE_NOTES,
  ADD_NOTE,
  UPDATE_NOTE
} from "./note";

import {
  fetchTrashsRequest,
  restoreTrashRequest,
  FETCH_TRASHS,
  RESTORE_TRASH
} from "./trash";

import { userLoginRequest, userRegisterRequest, userInfoRequest } from "./user";

import {
  CREATE_TAG,
  FETCH_TAG,
  DELETE_TAG,
  UPDATE_TAG,
  GET_BYID,
  fetchTagRequest,
  updateTagRequest,
  createTagRequest,
  deleteTagRequest,
  getTagByIdRequest
} from "./tag";

export {
  fetchNotesRequest,
  fetchRemindersRequest,
  removeNoteRequest,
  addNoteRequest,
  updateNoteRequest,
  uploadImgRuImage,
  searchNotesRequest,
  FETCH_NOTES,
  SEARCH_NOTE,
  FETCH_REMINDER,
  REMOVE_NOTES,
  ADD_NOTE,
  UPDATE_NOTE,
  fetchTrashsRequest,
  restoreTrashRequest,
  FETCH_TRASHS,
  RESTORE_TRASH,
  userLoginRequest,
  userRegisterRequest,
  userInfoRequest,
  CREATE_TAG,
  FETCH_TAG,
  DELETE_TAG,
  UPDATE_TAG,
  GET_BYID,
  fetchTagRequest,
  updateTagRequest,
  createTagRequest,
  deleteTagRequest,
  getTagByIdRequest
};
