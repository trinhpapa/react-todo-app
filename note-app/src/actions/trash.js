import { resourceRequest } from "../core/apiHelper";

export const FETCH_TRASHS = "FETCH_TRASHS";
export const RESTORE_TRASH = "RESTORE_TRASH";

export const fetchTrashsRequest = () => {
  return dispatch => {
    return resourceRequest("trash", "GET", null)
      .then(res => res.json())
      .then(res => {
        dispatch(fetchTrashs(res));
      })
      .catch(res => console.log(res));
  };
};

export const fetchTrashs = trashs => {
  return {
    type: FETCH_TRASHS,
    trashs
  };
};

export const restoreTrashRequest = id => {
  return dispatch => {
    return resourceRequest(`trash/${id}`, "PUT", null)
      .then(res => {
        dispatch(restoreTrash(id));
      })
      .catch(res => console.log(res));
  };
};

export const restoreTrash = id => {
  return {
    type: RESTORE_TRASH,
    id
  };
};
