import "./NoteItem.css";
import React from "react";
import { connect } from "react-redux";
import noImage from "../../placeholder-image.png";
import { Popover, OverlayTrigger } from "react-bootstrap";
import {
  removeNoteRequest,
  restoreTrashRequest,
  updateNoteRequest,
  uploadImgRuImage
} from "../../actions";

class NoteItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editMode: false,
      noteId: props.noteId,
      noteTitle: props.noteTitle,
      noteContent: props.noteContent,
      imageUrl: props.imageUrl,
      selectedTags: props.noteTags,
      reminderDate: "",
      reminderTime: "",
      tasks: [],
      imageUploading: false
    };
    this.openImageRef = React.createRef();
  }

  static getDerivedStateFromProps(nextProps) {
    return {
      noteId: nextProps.noteId,
      noteTitle: nextProps.noteTitle,
      noteContent: nextProps.noteContent,
      imageUrl: nextProps.imageUrl,
      selectedTags: nextProps.noteTags
    };
  }

  onEditNote = () => {
    this.setState({ editMode: true });
  };

  onRemoveNote = () => {
    this.props.removeNote(this.state.noteId);
  };

  onRestoreNote = () => {
    this.props.restoreTrash(this.state.noteId);
  };

  onTitleChange = event => {
    this.setState({ noteTitle: event.target.value });
  };

  onContentChange = event => {
    this.setState({ noteContent: event.target.value });
  };

  onSaveChange = event => {
    event.preventDefault();
    var data = {
      id: this.state.noteId,
      detail: {
        imageUrl: this.state.imageUrl.trim(),
        title: this.state.noteTitle.trim(),
        content: this.state.noteContent.trim(),
        reminderTime: new Date(
          `${this.state.reminderDate} ${this.state.reminderTime}`
        )
      },
      tags: this.state.selectedTags
    };
    if (
      data.detail.imageUrl.match(/(https:\/\/i.imgur.com\/).*/g) ||
      data.detail.imageUrl === null
    ) {
      if (data.detail.title.length > 0) {
        this.props.saveChangeNote(data);
        this.setState({ editMode: false });
      }
    }
  };

  onBrowserImage = () => {
    this.openImageRef.current.click();
  };

  setFileUrlState = e => {
    this.setState({ imageUploading: true });
    this.setState({ imageUrl: e.target.result });
    uploadImgRuImage(e.target.result)
      .then(res => {
        this.setState({ imageUrl: res.data.link });
        this.setState({ imageUploading: false });
      })
      .catch(err => {
        console.log(err);
        this.setState({ imageUploading: false });
      });
  };

  onChangeImage = e => {
    if (e.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(e.target.files[0]);

      reader.onload = this.setFileUrlState;
    }
  };

  onTagChange = event => {
    const oldState = this.state.selectedTags;
    if (event.target.checked) {
      var itemTag = JSON.parse(event.target.value);
      var checkIndex = oldState.findIndex(e => e.id === itemTag.id);
      if (checkIndex === -1) {
        oldState.push(itemTag);
        this.setState({ selectedTags: oldState });
      }
    } else {
      var item = JSON.parse(event.target.value);
      oldState.splice(oldState.findIndex(e => e.id === item.id), 1);
      this.setState({ selectedTags: oldState });
    }
  };

  onPickDateChange = event => {
    this.setState({ reminderDate: event.target.value });
  };

  onPickTimeChange = event => {
    this.setState({ reminderTime: event.target.value });
  };

  popDatetimePicker = (
    <Popover>
      <Popover.Content>
        <span>Date:</span>
        <input
          type="date"
          className="form-control"
          onChange={this.onPickDateChange}
        />
        <span>Time:</span>
        <input
          type="time"
          className="form-control"
          onChange={this.onPickTimeChange}
        />
      </Popover.Content>
    </Popover>
  );

  popTagPicker = tags => {
    return (
      <Popover>
        <div className="Popover-title">Select tags:</div>
        <Popover.Content>
          {tags.map((tag, i) => {
            return (
              <label className="checkbox-container" key={i}>
                {tag.name}
                <input
                  type="checkbox"
                  onChange={this.onTagChange}
                  value={JSON.stringify(tag)}
                  checked={
                    this.state.selectedTags.findIndex(e => e.id === tag.id) ===
                    -1
                      ? false
                      : true
                  }
                />
                <span className="checkmark" />
              </label>
            );
          })}
        </Popover.Content>
      </Popover>
    );
  };

  render() {
    return (
      <div className="card text-left">
        {(() => {
          switch (this.state.editMode) {
            case true: {
              return (
                <div className="Note-item">
                  <form onSubmit={this.onSubmitNote}>
                    <input
                      type="file"
                      ref={this.openImageRef}
                      className="virtual-file-upload"
                      onChange={this.onChangeImage}
                    />
                    <div className="Note-image" onClick={this.onBrowserImage}>
                      <img
                        src={this.state.imageUrl || noImage}
                        alt="upload"
                        className={
                          this.state.imageUploading === true ? "uploading" : ""
                        }
                      />
                    </div>
                    <div className="Note-handle">
                      <input
                        name="title"
                        type="text"
                        ref="title"
                        defaultValue={this.state.noteTitle}
                        onChange={this.onTitleChange}
                        className="form-control form-control-sm mb-2"
                        placeholder="Note title"
                      />
                      <textarea
                        ref="content"
                        name="content"
                        defaultValue={this.state.noteContent}
                        onChange={this.onContentChange}
                        placeholder="Note content"
                        className="form-control form-control-sm"
                        rows="5"
                      />
                      <div>
                        <span className="Note-time">
                          {this.state.reminderDate === undefined
                            ? this.props.reminderTime.toDateTime()
                            : this.state.reminderDate.toDate() +
                              " " +
                              this.state.reminderDate.toTime()}
                        </span>
                      </div>
                      <div className="Note-tags">
                        {this.state.selectedTags.map((tag, i) => {
                          return (
                            <div className="Note-tag" key={i}>
                              <span>{tag.name}</span>
                            </div>
                          );
                        })}
                      </div>
                      <div className="Handle-function">
                        <OverlayTrigger
                          trigger="click"
                          placement="right"
                          overlay={this.popTagPicker(this.props.tags)}
                        >
                          <button type="button" className="Note-control-button">
                            <i className="fa fa-tags" />
                          </button>
                        </OverlayTrigger>
                        <OverlayTrigger
                          trigger="click"
                          placement="right"
                          overlay={this.popDatetimePicker}
                        >
                          <button type="button" className="Note-control-button">
                            <i className="fa fa-calendar" />
                          </button>
                        </OverlayTrigger>
                      </div>
                    </div>
                    <div className="Note-control justify-content-center">
                      <div>
                        <button
                          className="Note-control-button"
                          type="submit"
                          title="Restore"
                          onClick={this.onSaveChange}
                        >
                          <i className="fa fa-check" /> Save
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              );
            }

            default: {
              return (
                <div className="Note-item expand">
                  <div className="Note-image">
                    {(() => {
                      if (this.state.imageUrl !== undefined) {
                        return <img src={this.state.imageUrl} alt="upload" />;
                      }
                    })()}
                  </div>
                  <div className="Note-span">
                    <span className="Note-title">{this.state.noteTitle}</span>
                    <br />
                    <span className="Note-content">
                      {this.state.noteContent}
                    </span>
                    <div className="Note-tags">
                      {this.state.selectedTags.map((tag, i) => {
                        return (
                          <div className="Note-tag" key={i}>
                            <span>{tag.name}</span>
                          </div>
                        );
                      })}
                    </div>
                  </div>

                  <div className="Note-control">
                    {(() => {
                      switch (this.props.trashView) {
                        case true:
                          return (
                            <div>
                              <button
                                className="Note-control-button"
                                title="Restore"
                                onClick={this.onRestoreNote}
                              >
                                <i className="fa fa-repeat" />
                              </button>
                            </div>
                          );
                        default:
                          return (
                            <div>
                              <button
                                className="Note-control-button"
                                title="Edit"
                                onClick={this.onEditNote}
                              >
                                <i className="fa fa-pencil" />
                              </button>
                              <button
                                className="Note-control-button"
                                title="Remove"
                                onClick={this.onRemoveNote}
                              >
                                <i className="fa fa-times" />
                              </button>
                            </div>
                          );
                      }
                    })()}
                    <span className="Note-time">
                      {(() => {
                        switch (this.props.reminderTime) {
                          case undefined:
                            return "";
                          default:
                            return (
                              <div>
                                <i
                                  className="fa fa-calendar"
                                  aria-hidden="true"
                                />{" "}
                                {this.props.reminderTime.toDateTime()}
                              </div>
                            );
                        }
                      })()}
                    </span>
                  </div>
                </div>
              );
            }
          }
        })()}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tags: state.tags.data
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    removeNote: id => {
      dispatch(removeNoteRequest(id));
    },
    restoreTrash: id => {
      dispatch(restoreTrashRequest(id));
    },
    saveChangeNote: note => {
      dispatch(updateNoteRequest(note));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NoteItem);
