import React from "react";
import NoteItem from "../note-item/NoteItem";
import { connect } from "react-redux";
import { fetchTrashsRequest } from "../../actions";

class Trash extends React.Component {
  componentDidMount() {
    this.props.fetchAllTrashs();
  }

  render() {
    return (
      <div>
        {/* <h1 className="App-title">TRASH LIST</h1> */}
        <div className="Note-search">
          <form>
            <input
              type="text"
              className="input-box input-search"
              placeholder="Search"
            />
            <button className="Button-search">
              <i className="fa fa-search" />
            </button>
          </form>
        </div>
        <div className="Note-list">
          <div className="card-columns">
            {this.props.trashs.map((note, i) => {
              return (
                <NoteItem
                  key={i}
                  trashView={true}
                  noteId={note.id}
                  noteTitle={note.detail.title}
                  noteContent={note.detail.content}
                  imageUrl={note.detail.imageUrl}
                  reminderTime={note.detail.reminderTime}
                  noteTags={note.tags}
                />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { trashs: state.trashs.data };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllTrashs: () => {
      dispatch(fetchTrashsRequest());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Trash);
