import React from "react";
import "./login.css";
import image from "../../bot.png";
import bg from "../../6-01.png";
import * as Constant from "../../core/Constants";
import {
  userLoginRequest,
  userInfoRequest,
  userRegisterRequest
} from "../../actions";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      rePassword: "",
      firstname: "",
      lastname: "",
      status_error: false,
      status_label: "",
      loging: false,
      registering: false,
      controlMode: true //true for login, false for registration
    };
  }

  onUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  onPasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  onFirstnameChange = event => {
    this.setState({ firstname: event.target.value });
  };

  onLastnameChange = event => {
    this.setState({ lastname: event.target.value });
  };

  onRePasswordChange = event => {
    this.setState({ rePassword: event.target.value });
  };

  onLoginMode = () => {
    this.setState({
      username: "",
      password: "",
      rePassword: "",
      firstname: "",
      lastname: "",
      status_error: false,
      status_label: "",
      loging: false,
      registering: false,
      controlMode: true
    });
  };

  onRegisterMode = () => {
    this.setState({
      username: "",
      password: "",
      rePassword: "",
      firstname: "",
      lastname: "",
      status_error: false,
      status_label: "",
      loging: false,
      registering: false,
      controlMode: false
    });
  };

  onLoginSubmit = event => {
    event.preventDefault();
    this.setState({ loging: true });
    var data = {
      username: this.state.username,
      password: this.state.password
    };
    userLoginRequest(data)
      .then(resAuth => {
        if (resAuth.access_token === undefined) {
          console.log(resAuth);
          this.setState({ loging: false });
          this.setState({ status_error: true });
          this.setState({
            status_label: Constant.INVALID_USERNAME_PASSWORD
          });
        } else {
          console.log(resAuth.access_token);
          userInfoRequest(resAuth.access_token)
            .then(resInfo => {
              resInfo.auth = resAuth;
              resInfo = JSON.stringify(resInfo);
              window.localStorage.setItem(
                Constant.USER_STORAGE,
                resInfo.encodeUserInfo()
              );
              window.location.reload();
            })
            .catch(() => {
              this.setState({ loging: false });
              this.setState({ status_error: true });
              this.setState({
                status_label: Constant.LOGIN_ERROR
              });
            });
        }
      })
      .catch(() => {
        this.setState({ loging: false });
        this.setState({ status_error: true });
        this.setState({
          status_label: Constant.LOGIN_ERROR
        });
      });
  };

  onRegisterSubmit = event => {
    event.preventDefault();
    this.setState({ loging: true });
    if (this.state.password !== this.state.rePassword) {
      this.setState({ loging: false });
      this.setState({ status_error: true });
      this.setState({
        status_label: Constant.REPASSWORD_DONTMATCH
      });
      return;
    }
    if (
      this.state.username.length < 1 ||
      this.state.firstname.length < 1 ||
      this.state.lastname.length < 1 ||
      this.state.password.length < 1
    ) {
      this.setState({ loging: false });
      this.setState({ status_error: true });
      this.setState({
        status_label: Constant.INVALID_INPUT_ERROR
      });
      return;
    }
    var data = {
      username: this.state.username,
      password: this.state.password,
      firstname: this.state.firstname,
      lastname: this.state.lastname
    };
    userRegisterRequest(data).then(res => {
      if (!res.successful) {
        this.setState({ loging: false });
        this.setState({ status_error: true });
        this.setState({
          status_label: res.errorMessage
        });
      } else {
        this.setState({ loging: false });
        this.setState({ status_error: true });
        this.setState({
          status_label: "Đăng ký thành công!"
        });
      }
    });
  };

  render() {
    return (
      <div className="login-component">
        <div className="login-panel">
          <div className="login-backdrop">
            <div className="handle-mode">
              <button onClick={this.onLoginMode}>Login</button>
              <button onClick={this.onRegisterMode}>Register</button>
            </div>
            <span>Welcome to</span>
            <br />
            <span>
              <b>NOTEEEEE</b>
            </span>
            <br />
            <span className="mini-text">Take note, take care</span>
            <img src={bg} alt="background" />
          </div>
          <div
            className={
              this.state.controlMode === true
                ? "login-control show"
                : "login-control hide"
            }
          >
            <img src={image} alt="Logo" />
            <span className="title">Login</span>
            <form onSubmit={this.onLoginSubmit}>
              <input
                name="username"
                type="text"
                placeholder="Username"
                onChange={this.onUsernameChange}
                value={this.state.username}
              />
              <input
                name="password"
                type="password"
                placeholder="Password"
                onChange={this.onPasswordChange}
                value={this.state.password}
              />
              <button
                type="submit"
                className="buttonLogin"
                disabled={this.state.loging}
              >
                {this.state.loging === true ? "PLEASE WAIT..." : "LOGIN"}
              </button>

              <label
                className={
                  this.state.status_error === true
                    ? "label-result show"
                    : "label-result hide"
                }
              >
                {this.state.status_label}
              </label>
            </form>
          </div>
          <div
            className={
              this.state.controlMode === true
                ? "login-control hide"
                : "login-control show"
            }
          >
            <img src={image} alt="Logo" />
            <span className="title">Register</span>
            <form onSubmit={this.onRegisterSubmit}>
              <input
                name="username"
                type="text"
                placeholder="Username"
                onChange={this.onUsernameChange}
              />
              <input
                name="firstname"
                type="text"
                placeholder="First name"
                onChange={this.onFirstnameChange}
              />
              <input
                name="lastname"
                type="text"
                placeholder="Last name"
                onChange={this.onLastnameChange}
              />
              <input
                name="password"
                type="password"
                placeholder="Password"
                onChange={this.onPasswordChange}
              />
              <input
                name="re-password"
                type="password"
                placeholder="Re Password"
                onChange={this.onRePasswordChange}
              />
              <button
                type="submit"
                className="buttonLogin"
                disabled={this.state.loging}
              >
                {this.state.loging === true ? "PLEASE WAIT..." : "REGISTER"}
              </button>

              <label
                className={
                  this.state.status_error === true
                    ? "label-result show"
                    : "label-result hide"
                }
              >
                {this.state.status_label}
              </label>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
