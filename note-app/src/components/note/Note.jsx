import "./Note.css";
import React from "react";
import NoteItem from "../note-item/NoteItem";
import NoteControl from "../note-control/NoteControl";
import { connect } from "react-redux";
import { fetchNotesRequest, searchNotesRequest } from "../../actions";

class Note extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchString: ""
    };
  }

  componentDidMount() {
    this.props.fetchAllNotes();
  }

  onSearchInputChange = event => {
    this.setState({ searchString: event.target.value });
  };

  onSearchSubmit = event => {
    event.preventDefault();
    this.props.searchNotes(this.state.searchString);
  };

  render() {
    return (
      <div>
        {/* <h1 className="App-title">NOTE LIST</h1> */}
        <div className="Note-search">
          <form onSubmit={this.onSearchSubmit}>
            <input
              type="text"
              name="searchString"
              className="input-box input-search"
              placeholder="Search"
              onChange={this.onSearchInputChange}
            />
            <button className="Button-search">
              <i className="fa fa-search" />
            </button>
          </form>
        </div>
        <div className="Note-list">
          <div className="card-columns">
            <NoteControl controlMode="" />
            {this.props.notes.data.map((note, i) => {
              return (
                <NoteItem
                  key={i}
                  noteId={note.id}
                  noteTitle={note.detail.title}
                  noteContent={note.detail.content}
                  imageUrl={note.detail.imageUrl}
                  reminderTime={note.detail.reminderTime}
                  noteTags={note.tags}
                />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { notes: state.notes };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllNotes: () => {
      dispatch(fetchNotesRequest());
    },
    searchNotes: searchString => {
      dispatch(searchNotesRequest(searchString));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Note);
