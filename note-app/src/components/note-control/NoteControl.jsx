import React from "react";
import "./NoteControl.css";
import { connect } from "react-redux";
import { addNoteRequest, uploadImgRuImage } from "../../actions";
import noImage from "../../placeholder-image.png";
import { Popover, OverlayTrigger } from "react-bootstrap";

class NoteControl extends React.Component {
  constructor(props) {
    super(props);
    this.controlMode = props.controlMode;
    this.state = {
      handleMode: this.controlMode,
      noteId: props.noteId | "",
      noteTitle: props.noteTitle | "",
      noteContent: props.noteContent | "",
      imageUploading: false,
      imageUrl: "",
      reminderDate: "",
      reminderTime: "",
      selectedTags: [],
      tasks: []
    };

    this.openImageRef = React.createRef();
  }

  onAddNote = () => {
    this.setState({ handleMode: "add" });
  };

  onTitleChange = event => {
    this.setState({ noteTitle: event.target.value });
  };

  onContentChange = event => {
    this.setState({ noteContent: event.target.value });
  };

  resetState() {
    this.setState({ noteId: "" });
    this.setState({ noteTitle: "" });
    this.setState({ noteContent: "" });
    this.setState({ imageUploading: false });
    this.setState({ imageUrl: "" });
    this.setState({ selectedTags: [] });
    this.setState({ tasks: [] });
  }

  onSubmitNote = event => {
    event.preventDefault();
    var data = {
      detail: {
        imageUrl: this.state.imageUrl.trim(),
        title: this.state.noteTitle.trim(),
        content: this.state.noteContent.trim(),
        reminderTime: new Date(
          `${this.state.reminderDate} ${this.state.reminderTime}`
        )
      },
      tags: this.state.selectedTags
    };
    if (!this.state.imageUploading) {
      if (!data.detail.imageUrl.match(/(https:\/\/i.imgur.com\/).*/g)) {
        data.detail.imageUrl = null;
      }
      if (data.detail.title.length > 0) {
        this.props.addNote(data);
        this.setState({ handleMode: "" });
        this.resetState();
      }
    }
  };

  onBrowserImage = () => {
    this.openImageRef.current.click();
  };

  setFileUrlState = e => {
    this.setState({ imageUploading: true });
    this.setState({ imageUrl: e.target.result });
    uploadImgRuImage(e.target.result)
      .then(res => {
        this.setState({ imageUrl: res.data.link });
        this.setState({ imageUploading: false });
      })
      .catch(err => {
        console.log(err);
        this.setState({ imageUploading: false });
      });
  };

  onChangeImage = e => {
    if (e.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(e.target.files[0]);

      reader.onload = this.setFileUrlState;
    }
  };

  onTagChange = event => {
    const oldState = this.state.selectedTags;
    if (event.target.checked) {
      var itemTag = JSON.parse(event.target.value);
      var checkIndex = oldState.findIndex(e => e.id === itemTag.id);
      if (checkIndex === -1) {
        oldState.push(itemTag);
        this.setState({ selectedTags: oldState });
      }
    } else {
      var item = JSON.parse(event.target.value);
      oldState.splice(oldState.findIndex(e => e.id === item.id), 1);
      this.setState({ selectedTags: oldState });
    }
  };

  onPickDateChange = event => {
    this.setState({ reminderDate: event.target.value });
  };

  onPickTimeChange = event => {
    this.setState({ reminderTime: event.target.value });
  };

  popDatetimePicker = (
    <Popover>
      <Popover.Content>
        <span>Date:</span>
        <input
          type="date"
          className="form-control"
          onChange={this.onPickDateChange}
        />
        <span>Time:</span>
        <input
          type="time"
          className="form-control"
          onChange={this.onPickTimeChange}
        />
      </Popover.Content>
    </Popover>
  );

  popTagPicker = tags => {
    return (
      <Popover>
        <div className="Popover-title">Select tags:</div>
        <Popover.Content>
          {tags.map((tag, i) => {
            return (
              <label className="checkbox-container" key={i}>
                {tag.name}
                <input
                  type="checkbox"
                  onChange={this.onTagChange}
                  value={JSON.stringify(tag)}
                  checked={
                    this.state.selectedTags.findIndex(e => e.id === tag.id) ===
                    -1
                      ? false
                      : true
                  }
                />
                <span className="checkmark" />
              </label>
            );
          })}
        </Popover.Content>
      </Popover>
    );
  };

  render() {
    return (
      <div className="card text-left">
        {(() => {
          switch (this.state.handleMode) {
            case "add": {
              return (
                <div className="Note-item">
                  <form onSubmit={this.onSubmitNote}>
                    <input
                      type="file"
                      ref={this.openImageRef}
                      className="virtual-file-upload"
                      onChange={this.onChangeImage}
                    />
                    <div className="Note-image" onClick={this.onBrowserImage}>
                      <img
                        src={this.state.imageUrl || noImage}
                        alt="upload"
                        className={
                          this.state.imageUploading === true ? "uploading" : ""
                        }
                      />
                    </div>
                    <div className="Note-handle">
                      <input
                        type="text"
                        ref="title"
                        name="title"
                        onChange={this.onTitleChange}
                        className="form-control form-control-sm mb-2"
                        placeholder="Note title"
                      />
                      <textarea
                        ref="content"
                        name="content"
                        placeholder="Note content"
                        onChange={this.onContentChange}
                        className="form-control form-control-sm"
                        rows="5"
                      />
                      <div>
                        <span className="Note-time">
                          {this.state.reminderDate === ""
                            ? ""
                            : (
                                this.state.reminderDate +
                                " " +
                                this.state.reminderTime
                              ).toDateTime()}
                        </span>
                      </div>
                      <div className="Note-tags">
                        {this.state.selectedTags.map((tag, i) => {
                          return (
                            <div className="Note-tag" key={i}>
                              <span>{tag.name}</span>
                            </div>
                          );
                        })}
                      </div>
                      <div className="Handle-function">
                        <OverlayTrigger
                          trigger="click"
                          overlay={this.popTagPicker(this.props.tags)}
                        >
                          <button type="button" className="Note-control-button">
                            <i className="fa fa-tags" />
                          </button>
                        </OverlayTrigger>
                        <OverlayTrigger
                          trigger="click"
                          overlay={this.popDatetimePicker}
                        >
                          <button type="button" className="Note-control-button">
                            <i className="fa fa-calendar" />
                          </button>
                        </OverlayTrigger>
                      </div>
                    </div>
                    <div className="Note-control justify-content-center">
                      <div className="">
                        <button
                          className="Note-control-button"
                          title="Restore"
                          onClick={this.submitNote}
                        >
                          <i className="fa fa-plus" /> Add
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              );
            }

            default: {
              return (
                <div className="Note-item Add-note" onClick={this.onAddNote}>
                  <div className="Add-note-content">
                    <i className="fa fa-plus-circle" />
                    <br />
                    <span>Add Note</span>
                  </div>
                </div>
              );
            }
          }
        })()}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tags: state.tags.data
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    addNote: note => {
      dispatch(addNoteRequest(note));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NoteControl);
