import "./Tag.css";
import React from "react";
import { connect } from "react-redux";
import NoteItem from "../note-item/NoteItem";
import { getTagByIdRequest } from "../../actions";

export class Tag extends React.Component {
  componentDidMount() {
    this.props.getTag(this.props.match.params.tagId);
  }

  componentDidUpdate = prevProps => {
    if (this.props.match.params.tagId !== prevProps.match.params.tagId) {
      this.props.getTag(this.props.match.params.tagId);
    }
  };

  render() {
    return (
      <div>
        <div className="Note-search">
          <form onSubmit={this.onSearchSubmit}>
            <input
              type="text"
              name="searchString"
              className="input-box input-search"
              placeholder="Search"
              onChange={this.onSearchInputChange}
            />
            <button className="Button-search">
              <i className="fa fa-search" />
            </button>
          </form>
        </div>
        <div className="Note-list">
          <div className="card-columns">
            {this.props.notes.map((note, i) => {
              if (note.meta.isDeleted === false) {
                return (
                  <NoteItem
                    key={i}
                    noteId={note.id}
                    noteTitle={note.detail.title}
                    noteContent={note.detail.content}
                    imageUrl={note.detail.imageUrl}
                    reminderTime={note.detail.reminderTime}
                    noteTags={note.tags}
                  />
                );
              } else {
                return "";
              }
            })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    notes: state.tags.item.notes === undefined ? [] : state.tags.item.notes
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getTag: id => {
      dispatch(getTagByIdRequest(id));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tag);
