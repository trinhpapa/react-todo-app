import React from "react";
import NoteItem from "../note-item/NoteItem";
import { connect } from "react-redux";
import { fetchRemindersRequest } from "../../actions";

class Reminder extends React.Component {
  componentDidMount() {
    this.props.fetchReminders();
  }

  onSearchSubmit = event => {
    event.preventDefault();
  };

  render() {
    return (
      <div>
        {/* <h1 className="App-title">NOTE LIST</h1> */}
        <div className="Note-search">
          <form onSubmit={this.onSearchSubmit}>
            <input
              type="text"
              className="input-box input-search"
              placeholder="Search"
            />
            <button className="Button-search">
              <i className="fa fa-search" />
            </button>
          </form>
        </div>
        <div className="Note-list">
          <div className="card-columns">
            {this.props.reminder.data.map((note, i) => {
              return (
                <NoteItem
                  key={i}
                  noteId={note.id}
                  noteTitle={note.detail.title}
                  noteContent={note.detail.content}
                  imageUrl={note.detail.imageUrl}
                  reminderTime={note.detail.reminderTime}
                  noteTags={note.tags}
                />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { reminder: state.notes };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchReminders: () => {
      dispatch(fetchRemindersRequest());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reminder);
