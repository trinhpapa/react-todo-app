import React from "react";
import { NavLink } from "react-router-dom";
import { Modal, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { createTagRequest, deleteTagRequest } from "../../actions";

class NavMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalEditTag: false,
      tagName: "",
      selectedTags: []
    };
  }

  onTagNameChange = event => {
    this.setState({ tagName: event.target.value });
  };

  submitCreateTag = () => {
    if (this.state.tagName.trim() !== "") {
      const data = {
        name: this.state.tagName.trim()
      };
      this.props.addTag(data);
    }
  };

  submitRemoveTag = () => {
    if (this.state.selectedTags.length > 0) {
      this.props.deleteSelectedTag(this.state.selectedTags);
    }
  };

  onTagChange = event => {
    const oldState = this.state.selectedTags;
    if (event.target.checked) {
      let itemTag = event.target.value;
      let checkIndex = oldState.findIndex(e => e === itemTag);
      if (checkIndex === -1) {
        oldState.push(itemTag);
        this.setState({ selectedTags: oldState });
      }
    } else {
      let item = event.target.value;
      oldState.splice(oldState.findIndex(e => e === item), 1);
      this.setState({ selectedTags: oldState });
    }
  };

  toClosePopTag = () => {
    this.setState({ modalEditTag: false });
  };

  toOpenPopTag = () => {
    this.setState({ modalEditTag: true });
  };

  render() {
    return (
      <div className="Menu-link">
        <Modal
          show={this.state.modalEditTag}
          onHide={this.close}
          centered
          backdrop="static"
          size="sm"
        >
          <Modal.Body>
            <span className="Modal-title">Edit Tags</span>
            <hr />
            {this.props.tags.data.map((tag, i) => {
              return (
                <label className="checkbox-container" key={i}>
                  {tag.name}
                  <input
                    type="checkbox"
                    onChange={this.onTagChange}
                    value={tag.id}
                    checked={
                      this.state.selectedTags.findIndex(e => e === tag.id) ===
                      -1
                        ? false
                        : true
                    }
                  />
                  <span className="checkmark" />
                </label>
              );
            })}

            <input
              className="form-control form-control-sm"
              placeholder="Enter tag name"
              onChange={this.onTagNameChange}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button
              size="sm"
              className="btn btn-danger"
              onClick={this.submitRemoveTag}
            >
              Remove selected
            </Button>
            <Button
              size="sm"
              className="btn btn-success"
              onClick={this.submitCreateTag}
            >
              Add tag
            </Button>
            <Button onClick={this.toClosePopTag} size="sm">
              Close
            </Button>
          </Modal.Footer>
        </Modal>
        <NavLink className="App-link" activeClassName="active" to="/note">
          <i className="fa fa-flag" /> Note
        </NavLink>
        <NavLink className="App-link" activeClassName="active" to="/reminders">
          <i className="fa fa-bell" /> Reminders
        </NavLink>
        <div className="Menu-title">
          <span>TAGs</span>
        </div>
        {this.props.data.map((tag, i) => {
          return (
            <NavLink
              key={i}
              className="App-link"
              activeClassName="active"
              to={`/tag/${tag.id}`}
            >
              <i className="fa fa-tags" /> {tag.name}
            </NavLink>
          );
        })}
        <a className="App-link" onClick={this.toOpenPopTag}>
          <i className="fa fa-pencil" /> Edit tags
        </a>
        <div className="Menu-title" />
        <NavLink className="App-link" activeClassName="active" to="/trash">
          <i className="fa fa-trash" /> Trash
        </NavLink>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tags: state.tags
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addTag: tag => {
      dispatch(createTagRequest(tag));
    },
    deleteSelectedTag: selectedTag => {
      dispatch(deleteTagRequest(selectedTag));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavMenu);
